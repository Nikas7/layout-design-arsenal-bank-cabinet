
$('body').on('click', function(e) {
    var attrButton;

    if(!$(e.target).closest('#lang-switch').length && $('#lang-switch .dropdown-menu').is(":visible")) {
        $('#lang-switch .dropdown-menu').fadeOut(200);
        $('#lang-switch .btn-lang').attr('aria-expanded', false);
    }

    if(!$(e.target).closest('#number-switch').length && $('#number-switch .dropdown-menu').is(":visible")) {
        $('#number-switch .dropdown-menu').fadeOut(200);
        $('#number-switch .btn-lang').attr('aria-expanded', false);
    }

    if ($(e.target).hasClass('btn-lang') || $(e.target).parents().hasClass('btn-lang')) {

        if ($(e.target).closest('.dropdown').attr('id') == 'lang-switch') {
            attrButton = $('#lang-switch');
        }

        if ($(e.target).closest('.dropdown').attr('id') == 'number-switch') {
            attrButton = $('#number-switch');
        }

        if (attrButton.find('.btn-lang').attr('aria-expanded') == 'true') {
            attrButton.find('.btn-lang').attr('aria-expanded', false);
        } else if (attrButton.find('.btn-lang').attr('aria-expanded') == 'false') {
            attrButton.find('.btn-lang').attr('aria-expanded', true);
        }

        attrButton.find('.dropdown-menu').fadeToggle(200);
    }
})
const hamb = document.querySelector("#hamb");
const popup = document.querySelector("#popup");
const body = document.body;

// Клонируем меню, чтобы задать свои стили для мобильной версии
const menu = document.querySelector("#menu").cloneNode(1);

// При клике на иконку hamb вызываем ф-ию hambHandler
hamb.addEventListener("click", hambHandler);

// Выполняем действия при клике ..
function hambHandler(e) {
  e.preventDefault();
  // Переключаем стили элементов при клике
  popup.classList.toggle("open");
  hamb.classList.toggle("active");

  renderPopup();
}

// Здесь мы рендерим элементы в наш попап
function renderPopup() {
  popup.appendChild(menu);
}

// Код для закрытия меню при нажатии на ссылку
const links = Array.from(menu.children);

// Для каждого элемента меню при клике вызываем ф-ию
links.forEach((link) => {
  link.addEventListener("click", closeOnClick);
});

// Закрытие попапа при клике на меню
function closeOnClick() {
  popup.classList.remove("open");
  hamb.classList.remove("active");

}

$('#hamb-close').click (function(){
$('.popup').removeClass("open");
});
$('.btn-lang-mob').click(function(){
$('.dropdown-menu-lang').show();
});
$('.title-show').click (function(){
    $( this ).toggleClass('show');
});
document.querySelectorAll(".clear_input")
.forEach(function (elem) {
  elem.onclick = function (e) {
    let selector = this.dataset.clearSelector;
    document.querySelectorAll(selector)
    .forEach(function (item) {
      item.value = "";
    });
  };
});
